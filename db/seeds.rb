# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create!([{username: "User1", email: "user1@gmail.com", password: "pass"},
              {username: "User2", email: "user2@gmail.com", password: "pass"},
              {username: "User3", email: "user3@gmail.com", password: "pass"}])

Band.create!([{band_name: "Band1"},
              {band_name: "Band2"},
              {band_name: "Band3"}])

Album.create!([{album_name: "Album1", band_id: 1},
               {album_name: "Album2", band_id: 1},
               {album_name: "Album3", band_id: 1}])

Track.create!([{track_name: "Track1", album_id: 1},
               {track_name: "Track2", album_id: 1},
               {track_name: "Track3", album_id: 1}])

Note.create!([{text: "Cool Track From User1", track_id: 1, user_id: 1},
              {text: "Cool Track From User2", track_id: 1, user_id: 2},
              {text: "Cool Track From User3", track_id: 1, user_id: 3}])