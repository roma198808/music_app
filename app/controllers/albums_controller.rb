class AlbumsController < ApplicationController
  def index
    @albums = Album.all
  end

  def show
    @album  = Album.find(params[:id])
    @band   = @album.band
    @tracks = @album.tracks
  end

  def create
    @album = Album.new(params[:album])

    flash[:notice] = (@album.save) ? "Album added" : "Failure"
    redirect_to :back
  end

  def destroy
    @album = Album.find(params[:id])
    @band  = @album.band

    flash[:notice] = (@album.destroy) ? "Album removed" : "Failure"
    redirect_to band_url(@band.id)
  end
end
