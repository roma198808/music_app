class NotesController < ApplicationController
  def show
    @note  = Note.find(params[:id])
    @track = @note.track
    @album = @note.album
    @band  = @note.band
    @user  = @note.user
  end

  def create
    @note = Note.new(params[:note])

    flash[:notice] = (@note.save) ? "Note added" : "Failure"
    redirect_to :back
  end

  def destroy
    @note = Note.find(params[:id])
    @track = @note.track

    flash[:notice] = (@note.destroy) ? "Track removed" : "Failure"
    redirect_to track_url(@track.id)
  end
end
