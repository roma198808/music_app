class TracksController < ApplicationController
  def show
    @track = Track.find(params[:id])
    @album = @track.album
    @band  = @track.band
    @notes = @track.notes
  end

  def create
    @track = Track.new(params[:track])

    flash[:notice] = (@track.save) ? "Track added" : "Failure"
    redirect_to :back
  end

  def destroy
    @track = Track.find(params[:id])
    @album = @track.album

    flash[:notice] = (@track.destroy) ? "Track removed" : "Failure"
    redirect_to album_url(@album.id)
  end
end
