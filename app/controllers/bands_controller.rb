class BandsController < ApplicationController
  def index
    @bands  = Band.all
  end

  def show
    @band   = Band.find(params[:id])
    @albums = @band.albums
  end

  def create
    @band = Band.new(params[:band])

    flash[:notice] = (@band.save) ? "Band added" : "Failure"
    redirect_to :back
  end

  def destroy
    @band = Band.find(params[:id])

    flash[:notice] = (@band.destroy) ? "Band removed" : "Failure"
    redirect_to bands_url
  end
end
