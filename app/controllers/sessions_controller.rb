class SessionsController < ApplicationController
  def new
  end

  def create
    @user = User.find_by_credentials(params[:user][:username],
                                     params[:user][:password])

    if @user
      login(@user)
      flash[:notice] = "Logged in"
      redirect_to user_url(@user)
    else
      flash[:notice] = "Failure"
      redirect_to login_page_url
    end
  end

  def destroy
    logout
    flash[:notice] = "Logged out"
    redirect_to root_url
  end
end
