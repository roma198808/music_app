class UsersController < ApplicationController
  before_filter :require_current_user!, only: [:show]

  def new
    @user = User.new
  end

  def create
    @user = User.new(params[:user])

    if UserMailer.welcome_email(@user).deliver
      flash[:notice] = "Mail sent"
      redirect_to root_url
    else
      flash[:notice] = "Failure"
    redirect_to root_url
    end
  end

  def show
    redirect_to root_url if current_user.id != params[:id].to_i
    @user = current_user
    @notes = @user.notes
  end

  def activate

  end
end
