class User < ActiveRecord::Base
  attr_accessible :username, :email, :password
  attr_reader :password

  before_validation :ensure_session_token
  has_many :notes, dependent: :destroy

  def self.find_by_credentials(username, password)
    @user = User.find_by_username(username)
    return nil unless @user
    BCrypt::Password.new(@user.pass_digest).is_password?(password) ? @user : nil
  end

  def ensure_session_token
    self.session_token ||= SecureRandom.urlsafe_base64(16)
  end

  def password=(password)
    @password = password
    self.pass_digest = BCrypt::Password.create(password)
  end

  def reset_session_token!
    self.session_token = SecureRandom.urlsafe_base64(16)
    self.save!
  end
end
