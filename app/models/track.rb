class Track < ActiveRecord::Base
  attr_accessible :track_name, :album_id

  belongs_to :album
  has_one    :band, through: :album
  has_many   :notes, dependent: :destroy
end
