class Note < ActiveRecord::Base
  attr_accessible :text, :track_id, :user_id

  belongs_to :track
  belongs_to :user

  has_one :album, through: :track
  has_one :band,  through: :album
end
